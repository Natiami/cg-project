﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{

    private const string RATE_URL = "https://www.facebook.com/";

    public void OnPlayClick()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
    }

    public void OnRateClick()
    {
        Application.OpenURL(RATE_URL);
    }

    public void SoundClick()
    {

    }

 
}

