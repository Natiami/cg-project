﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoSingleton<Ball>
{
    public Text scoreText;

    private const float DEADZONE = 30.0f; 
    private const float MAXPULL = 100.0f;

    private const float LOSE_Y = -0.5f;

    private Vector2 landingPosition;
    public int amountBalls = 1;
    private int amountBallsLeft;

    private bool isBumping;
    private bool firstBallLanded;
    private Rigidbody2D rigid;
    public float speed = 8.0f;
    public Transform rowContainer;
    public GameObject rowPrefab;
    public GameObject tutorial;

    public DateTime hittingStart;

    private int score;


    private float currentSpawnY;

    public Transform ballsPreview;

   

    // Use this for initialization
    private void Start()
    {
        Debug.Log("Start " + this.GetType().ToString());
        rigid = GetComponent<Rigidbody2D>();
        ballsPreview.parent.gameObject.SetActive(false);
        amountBallsLeft = amountBalls;
    }

    // Update is called once per frame
    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Alpha1))
        //    GenerateNewRow();

        if (!isBumping)
            PoolInput();

    }

    private void PoolInput()
    {
        // Drag the ball
        //Debug.Log(MobileInput.Instance.swipeDelta);
        Vector3 swipeDelt = MobileInput.Instance.swipeDelta;
        // if we want to pool other way around
        swipeDelt.Set(-swipeDelt.x, -swipeDelt.y, swipeDelt.z);

        if (swipeDelt != Vector3.zero)
        {
            //Check swiping direction, if we swipe down no line shown
            swipeDelt.y = Mathf.Max(5.4f, swipeDelt.y);
            //if (swipeDelt.y < 0)
            //{
            //    swipeDelt.y = 0.05f;
                //ballsPreview.parent.gameObject.SetActive(false);
            //}
 
                ballsPreview.parent.up = swipeDelt.normalized; // Rotate the preview
                ballsPreview.parent.gameObject.SetActive(true);
                ballsPreview.localScale = Vector3.Lerp(new Vector3(1, 1, 1), new Vector3(1, 3, 1), swipeDelt.magnitude / MAXPULL);
                if (MobileInput.Instance.release)
                {
                    tutorial.SetActive(false);
                    isBumping = true;
                    SendBallinDirection(swipeDelt.normalized);
                    ballsPreview.parent.gameObject.SetActive(false);
                    rigid.simulated = true;
                }
            
        }

       
    }

    private void SendBallinDirection(Vector3 dir)
    {
        rigid.velocity = dir * speed;
        hittingStart = DateTime.Now;
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Floor")
            TouchFloor();
        else if(coll.gameObject.tag == "Block")
        {
            // cal ReceiveHit funtion inside block
            coll.gameObject.SendMessage("ReceiveHit");
        }
        //Debug.Log("Collision");
        double span = (DateTime.Now - hittingStart).TotalSeconds;
        if (span > 20)
        {
            Debug.Log( rigid.position);
            TouchFloor();
        }
    }

    private void TouchFloor()
    {
        amountBallsLeft--;

        if (!firstBallLanded)
        {
            firstBallLanded = true;
            rigid.velocity = Vector2.zero;
            rigid.simulated = false;
        }
        if (amountBallsLeft == 0)
            AllBallLanded(); // Throw ball again
    }

    private void AllBallLanded()
    {
        amountBallsLeft = 1; //amountBalls;
        firstBallLanded = false;
        isBumping = false;
        BlockContainer.Instance.GenerateNewRow();
        score++;
        UpdateText();
    }

    public void UpdateText()
    {
        scoreText.text = score.ToString(); 
    }
}
