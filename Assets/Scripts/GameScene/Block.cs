﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Block : MonoBehaviour
{
    public int healthPoint;
    public Text healthPointText;

    //public List<Color> colors = new List<Color>();
    public static List<Color> blockColors = new List<Color>() {
        new Color(1.0f, 0.3f, 1.0f), // 1
        new Color(0.7f, 0.3f, 1.0f), // 2
        new Color(0.4f, 0.3f, 1.0f), // 3
        new Color(1.0f, 1.0f, 0.5f),
        new Color(0.5f, 1.0f, 1.0f),
        new Color(0.5f, 0.5f, 0.5f)
    };


    public SpriteRenderer spriteRenderer;

    public void Spawn()
    {
        int ballAmount = Ball.Instance.amountBalls;
        healthPoint = UnityEngine.Random.Range(ballAmount - 3, ballAmount + 3);
        
        if (healthPoint <= 0)
            healthPoint = 1;
        //UpdateText();
        UpdateColor();

    }

    public void ReceiveHit()
    {
        healthPoint--;
        UpdateColor();
        if (healthPoint <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    protected void UpdateColor()
    {
        //Debug.Log(healthPoint - 1);
        if (healthPoint > 0)
        {
            spriteRenderer.color = blockColors[healthPoint - 1];
        }
    }

    public void Hide()
    {
        Destroy(this.gameObject);
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        Debug.Log("Collision");
        //if (coll.gameObject.tag == "Floor")
        //{
        //    Debug.Log("Floor");
        //    OnGameOver();
        //}
        //else if (coll.gameObject.tag == "Ball")
        //{
        //    Debug.Log("Ball");
        //}
    }

    private void OnGameOver()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
        //Application.LoadLevel("Menu");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Trigger");
        OnGameOver();
    }


    public void UpdateText()
    {
        healthPointText.text = healthPoint.ToString();
    }
}