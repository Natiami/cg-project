﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockContainer : MonoSingleton<BlockContainer> {

    private List<Block> blocks = new List<Block>();
    public GameObject rowPrefab;
    public Transform rowContainer;
    private float currentSpawnY;
    private Vector2 desiredPosition;
    private Vector2 rowContainerStartingPosition;

    private const float DISTANCE_BETWEEN_BLOCKS = 0.37f;

    public void GenerateNewRow()
    {
        //go.transform.localPosition = Vector2.down* currentSpawnY;
        //currentSpawnY -= DISTANCE_BETWEEN_BLOCKS;
        // desiredPosition = rowContainerStartingPosition + (Vector2.up * currentSpawnY);

        GameObject go = Instantiate(rowPrefab) as GameObject;
        go.transform.SetParent(rowContainer);
        go.transform.localPosition = Vector2.up * currentSpawnY;
        currentSpawnY += DISTANCE_BETWEEN_BLOCKS;
        //Debug.Log("GenerateNewRow" + currentSpawnY);

        desiredPosition = rowContainerStartingPosition + (Vector2.down * currentSpawnY);

        Block[] row = go.GetComponentsInChildren<Block>();
        for (int i = 0; i < row.Length; i++)
        {
            // not show the bricks everywhere but only some places on the row
            if (UnityEngine.Random.Range(0f, 1f) > 0.6f)
                row[i].Spawn();
            else row[i].Hide();
        }
    }

	// Use this for initialization
	private void Start () {
        Debug.Log("Start " + this.GetType().ToString());
        rowContainerStartingPosition = rowContainer.transform.position;
        desiredPosition = rowContainerStartingPosition;
    }
	
	// Update is called once per frame
	void Update () {
        if ((Vector2)rowContainer.position != desiredPosition)
        {
            rowContainer.transform.position = Vector3.MoveTowards(rowContainer.transform.position, 
                                                                    desiredPosition, 
                                                                    Time.deltaTime);
            //Debug.Log("desiredPosition" + desiredPosition);
        }
    }
}
