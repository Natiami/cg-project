﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileInput : MonoSingleton<MobileInput>
{

    public bool tap, release, hold;
    public Vector2 swipeDelta;
    private Vector2 initialPosition;


    // Use this for initialization
    private void Start () {
        Debug.Log("Start " + this.GetType().ToString());
    }
	
	// Update is called once per frame
	private void Update () {

        release = tap = false;
        swipeDelta = Vector2.zero;

        if (Input.GetMouseButtonDown(0))
        {
            hold = tap = true;
            initialPosition = Input.mousePosition;
           // Debug.Log(initialPosition);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            release = true;
            hold = false;
            swipeDelta = (Vector2)Input.mousePosition - initialPosition;
        }
        if (hold)
        {
            // Drag
            swipeDelta = (Vector2)Input.mousePosition - initialPosition;
        }
	}
}
